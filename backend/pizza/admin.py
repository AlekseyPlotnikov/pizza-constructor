from django.contrib import admin
from .models import Pizza, Topping, Dough, Sauce

admin.site.register(Topping)


class PizzaAuthorInLine(admin.TabularInline):
    model = Pizza
    extra = 1
    fk_name = 'author_pizza'


class PizzaDoughInLine(admin.TabularInline):
    model = Pizza
    extra = 1
    fk_name = 'dough_pizza'


class PizzaSauceInLine(admin.TabularInline):
    model = Pizza
    extra = 1
    fk_name = 'sauce_pizza'


@admin.register(Pizza)
class PizzaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'dough_pizza', 'sauce_pizza', 'author_pizza')
    prepopulated_fields = {
        'slug': ('title_pizza',)
    }


@admin.register(Dough)
class DoughAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'size_dough')
    inlines = [PizzaDoughInLine, ]


@admin.register(Sauce)
class SauceAdmin(admin.ModelAdmin):
    list_display = ('__str__',)
    inlines = [PizzaSauceInLine, ]
