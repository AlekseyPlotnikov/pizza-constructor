from django import forms
from .models import Pizza, Dough, Topping, Sauce


class PizzaForm(forms.ModelForm):
    class Meta:
        model = Pizza
        fields = '__all__'


class DoughForm(forms.ModelForm):
    class Meta:
        model = Dough
        fields = '__all__'


class ToppingForm(forms.ModelForm):
    class Meta:
        model = Topping
        fields = '__all__'


class SauceForm(forms.ModelForm):
    class Meta:
        model = Sauce
        fields = '__all__'
