from django.db import models
from django.utils.timezone import now

from django.conf import settings


class Dough(models.Model):
    title_dough = models.CharField(max_length=50, verbose_name='Разновидность теста')
    text_dough = models.TextField(blank=True, verbose_name='Описание теста')
    size_dough = models.IntegerField(verbose_name='размер теста')

    def name_related(self):
        return self.dough_pizza.all()

    def __str__(self):
        return f'{self.title_dough}'

    class Meta:
        verbose_name = 'Тесто'
        verbose_name_plural = 'Тесто'


class Sauce(models.Model):
    title_sauce = models.CharField(max_length=50, verbose_name=' Название соус')
    text_sauce = models.TextField(blank=True, verbose_name='Описание соуса')

    def name_related(self):
        return self.sauce_pizza.all()

    def __str__(self):
        return f'{self.title_sauce}'

    class Meta:
        verbose_name = 'Соус'
        verbose_name_plural = 'Соус'


class Topping(models.Model):
    title_topping = models.CharField(max_length=50, verbose_name='Название добавки')
    text_topping = models.TextField(blank=True, verbose_name='Описание добавки')
    image_topping = models.ImageField(verbose_name='Фото', blank=True)

    def name_related(self):
        return self.topping_pizza.all()

    def __str__(self):
        return f'{self.title_topping}'

    class Meta:
        verbose_name = 'Добавка'
        verbose_name_plural = 'Добавки'


class Pizza(models.Model):
    title_pizza = models.CharField(max_length=50, verbose_name='Название рецепта')
    text_pizza = models.TextField(blank=True, verbose_name='Описание рецепта')
    dough_pizza = models.ForeignKey(Dough, on_delete=models.CASCADE, related_name='dough_pizza',
                                    verbose_name='тесто', null=True)
    sauce_pizza = models.ForeignKey(Sauce, on_delete=models.CASCADE, related_name='sauce_pizza', verbose_name='соус')
    topping_pizza = models.ManyToManyField(Topping, related_name='topping_pizza', verbose_name='добавка', blank=True,
                                           null=True)
    author_pizza = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='автор рецепта')
    created_pizza = models.DateTimeField(verbose_name='Дата создания рецепта', default=now)

    image_pizza = models.ImageField(verbose_name='Фото', blank=True)
    slug = models.SlugField(verbose_name='Слаг', blank=True, unique=True, null=True)

    def __str__(self):
        return f'{self.title_pizza}'

    class Meta:
        verbose_name = 'Пицца'
        verbose_name_plural = 'Пиццы'
