# Generated by Django 3.2.9 on 2021-11-12 20:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pizza', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pizza',
            name='image_pizza',
            field=models.ImageField(blank=True, upload_to='', verbose_name='Фото'),
        ),
    ]
