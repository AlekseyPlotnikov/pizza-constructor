from django.urls import path
from . import views
from .views import SearchToppingResultView, search_result_view

app_name = "pizza"


urlpatterns = [
    path('', views.pizzas, name = "pizzas"),
    path("pizzas/<slug>/", views.pizza, name = "pizza"),
    path("dough/", views.dough, name = "dough"),
    path("sauce/", views.sauce, name = "sauce"),
    path("topping/", views.topping, name = "topping"),
    path("searchTopping/", views.SearchToppingResultView.as_view(), name ='searchTopping'),
    path("search/", views.search_result_view, name ='search'),







]

