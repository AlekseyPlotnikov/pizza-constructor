from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from pizza.models import *

User = get_user_model()


class PizzaTestCase(TestCase):
    def setUp(self):
        d1 = Dough.objects.create(title_dough='test_title_dough', text_dough='test_text_dough', size_dough=25)
        s1 = Sauce.objects.create(title_sauce='test_title_sauce', text_sauce='test_text_sauce')
        t1 = Topping.objects.create(title_topping='test_title_topping', text_topping='test_text_topping')
        u1 = User.objects.create()
        Pizza.objects.create(title_pizza='test_title_pizza', text_pizza='test_text_pizza', sauce_pizza=s1,
                             author_pizza=u1, slug='test-slug', dough_pizza=d1)

    def test_pizzas_view(self):
        c = Client()
        response = c.get("")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["pizzas"].count(), 1)

    def test_dough_view(self):
        c = Client()
        response = c.get("/dough/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["doughs"].count(), 1)

    def test_pizza_view(self):
        c = Client()
        response = c.get("/pizzas/test-slug/")
        self.assertEqual(response.status_code, 200)

    def test_sauce_view(self):
        c = Client()
        response = c.get("/sauce/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['sauces'].count(), 1)

    def test_topping_view(self):
        c = Client()
        response = c.get("/topping/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['toppings'].count(), 1)

    def test_search_result_view_view(self):
        c = Client()
        response = c.get("/search/")
        self.assertEqual(response.status_code, 200)
