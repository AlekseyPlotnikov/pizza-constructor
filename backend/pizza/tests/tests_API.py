from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from pizza.models import *

User = get_user_model()


class PizzaTestCase(TestCase):
    def setUp(self):
        d1 = Dough.objects.create(title_dough='test_title_dough', text_dough='test_text_dough', size_dough=25)
        s1 = Sauce.objects.create(title_sauce='test_title_sauce', text_sauce='test_text_sauce')
        t1 = Topping.objects.create(title_topping='test_title_topping', text_topping='test_text_topping')
        u1 = User.objects.create()
        Pizza.objects.create(title_pizza='test_title_pizza', text_pizza='test_text_pizza', sauce_pizza=s1,
                             author_pizza=u1, slug='test-slug', dough_pizza=d1)

    def test_PizzaViewSet_api_get(self):
        c = Client()
        response = c.get("/api/v1/pizzas/1/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['title_pizza'], 'test_title_pizza')
        self.assertEqual(response.json()['text_pizza'], 'test_text_pizza')
        self.assertEqual(response.json()['sauce_pizza'], 1)
        self.assertEqual(response.json()['author_pizza'], 1)
        self.assertEqual(response.json()['slug'], 'test-slug')
        self.assertEqual(response.json()['dough_pizza'], 1)

    def test_PizzaViewSet_api_post(self):
        c = Client()
        data = {
            "title_pizza": "test_title_pizza2",
            "text_pizza": 'test_text_pizza2',
            'sauce_pizza': 1,
            'author_pizza': 1,
            "slug": 'test-slug2',
            'dough_pizza': 1,
        }
        response = c.post("/api/v1/pizzas/", data=data)
        self.assertEqual(response.status_code, 201)
