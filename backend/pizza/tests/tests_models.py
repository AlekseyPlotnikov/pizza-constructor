from django.test import TestCase
from django.contrib.auth import get_user_model

from pizza.models import *

User = get_user_model()


class PizzaTestCase(TestCase):
    def setUp(self):
        d1 = Dough.objects.create(title_dough='test_title_dough', text_dough='test_text_dough', size_dough=25)
        s1 = Sauce.objects.create(title_sauce='test_title_sauce', text_sauce='test_text_sauce')
        t1 = Topping.objects.create(title_topping='test_title_topping', text_topping='test_text_topping')
        u1 = User.objects.create()
        Pizza.objects.create(title_pizza='test_title_pizza', text_pizza='test_text_pizza', sauce_pizza=s1,
                             author_pizza=u1, slug='test-slug', dough_pizza=d1)

    def test_pizza_str(self):
        d1 = Dough.objects.get(title_dough='test_title_dough')
        s1 = Sauce.objects.get(title_sauce='test_title_sauce')
        t1 = Topping.objects.get(title_topping='test_title_topping')
        u1 = User.objects.get()
        p = Pizza.objects.create(title_pizza='test_title_pizza', text_pizza='test_text_pizza', sauce_pizza=s1,
                                 author_pizza=u1, slug='test-slug2', dough_pizza=d1)
        pizza_str = p.title_pizza
        self.assertEquals(pizza_str, str(p))

    def test_dough_str(self):
        d1 = Dough.objects.get(id=1)
        dough_str = d1.title_dough
        self.assertEquals(dough_str, str(d1))

    def test_sauce_str(self):
        s1 = Sauce.objects.get(id=1)
        dough_str = s1.title_sauce
        self.assertEquals(dough_str, str(s1))
