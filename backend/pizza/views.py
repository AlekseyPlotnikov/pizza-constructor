from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from .models import *
from .form import *
from django.views.generic import ListView


def pizzas(request):
    if request.method == "POST":
        pizza_form = PizzaForm(request.POST)
        if pizza_form.is_valid():
            pizza_form.save()
        else:
            pass
    p = Pizza.objects.order_by('-id')
    context = {
        "pizzas": p,
        'pizza_form': PizzaForm()
    }
    return render(request, 'pizza/pizzas.html', context)


# @login_required(login_url='users:signup')
def pizza(request, slug):
    p = get_object_or_404(Pizza, slug=slug)
    context = {
        'pizza': p,
    }
    return render(request, 'pizza/pizza.html', context)


def dough(request):
    if request.method == "POST":
        dough_form = DoughForm(request.POST)
        if dough_form.is_valid():
            dough_form.save()
        else:
            pass
    p = Dough.objects.order_by('-id')
    context = {
        "doughs": p,
        'dough_form': DoughForm()
    }
    return render(request, 'pizza/dough.html', context)


def sauce(request):
    if request.method == "POST":
        sauce_form = SauceForm(request.POST)
        if sauce_form.is_valid():
            sauce_form.save()
        else:
            pass
    s = Sauce.objects.order_by('-id')
    context = {
        "sauces": s,
        'sauce_form': SauceForm()
    }
    return render(request, 'pizza/sauce.html', context)


def topping(request):
    if request.method == "POST":
        topping_form = ToppingForm(request.POST)
        if topping_form.is_valid():
            topping_form.save()
        else:
            pass
    t = Topping.objects.order_by('-id')
    context = {
        "toppings": t,
        'topping_form': ToppingForm()
    }
    return render(request, 'pizza/topping.html', context)


class SearchToppingResultView(ListView):
    model = Pizza
    template_name = 'pizza/searchTopping.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Pizza.objects.filter(
            Q(topping_pizza__title_topping__icontains=query) | Q(sauce_pizza__title_sauce__icontains=query) |
            Q(dough_pizza__title_dough__icontains=query)
        )
        return object_list


# @login_required(login_url='users:signup')
def search_result_view(request):
    return render(request, 'pizza/search.html')
