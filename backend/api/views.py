

from rest_framework import viewsets
from pizza.models import *
from api.serializers import *


class PizzaViewSet(viewsets.ModelViewSet):
    queryset = Pizza.objects.all()
    serializer_class = PizzaSerializer

