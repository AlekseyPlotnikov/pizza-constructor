from api.views import *
from rest_framework import routers



pizza_router = routers.DefaultRouter()
pizza_router.register('pizzas', PizzaViewSet)

urlpatterns = []
urlpatterns += pizza_router.urls



